package mk.ukim.finki.exam_schedule.model;

public enum ExamType {
    LAB, CLASSROOM, ONLINE, HOMEWORK
}
