package mk.ukim.finki.exam_schedule.model;

public enum RoomType {

    CLASSROOM, LAB, MEETING_ROOM, OFFICE
}
