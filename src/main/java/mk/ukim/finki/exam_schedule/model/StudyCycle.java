package mk.ukim.finki.exam_schedule.model;

public enum StudyCycle {

    UNDERGRADUATE, MASTER, PHD
}
