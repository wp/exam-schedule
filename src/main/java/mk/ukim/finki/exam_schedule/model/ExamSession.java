package mk.ukim.finki.exam_schedule.model;

public enum ExamSession {
    FIRST_MIDTERM, SECOND_MIDTERM, JANUARY, JUNE, SEPTEMBER
}
