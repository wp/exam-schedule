package mk.ukim.finki.exam_schedule.model;

public enum SemesterType {
    WINTER,
    SUMMER
}
